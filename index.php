<?php
require __DIR__.'/vendor/autoload.php';
use Symfony\Component\Dotenv\Dotenv;

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://sonarqube.devops.stefanini.io/api/users/search",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic cmVwb3J0OlN0ZWZhbmluaUAxMA=="
  ),
));

$response = curl_exec($curl);
if(!$response){die('deu ruim');};

curl_close($curl);
echo $response;
?>